package bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	
	@Test
	public void testShouldReturnFrameAtAGivenIndex() throws BowlingException  {
		
		Game game = new Game();
		
		Frame secondFrame = new Frame(2, 5);
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(secondFrame);
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		
		assertEquals(secondFrame,game.getFrameAt(1));
		
		
	}

	@Test
	public void testShouldReturnTheScoreOfAGame() throws BowlingException{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		
		assertEquals(57,game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithASpareFrame() throws BowlingException{
		Game game = new Game();
		
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		
		assertEquals(88,game.calculateScore());
		
	}
	
	@Test
	public void testGameScoreWithAStrike() throws BowlingException{
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		
		assertEquals(94,game.calculateScore());
		
	}
	
	@Test
	public void testGameScoreWithStrikeAndSpare() throws BowlingException{
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithMultipleStrikes() throws BowlingException{
		Game game = new Game();
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithMultipleSpares() throws BowlingException{
		Game game = new Game();
		
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		
		assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnFirstBonusThrow() throws BowlingException{
		Game game = new Game();
		
		game.setFirstBonusThrow(7);

		assertEquals(7,game.getFirstBonusThrow());
	}
	
	@Test
	public void testGameScoreWithSpareAsTheLastFrame() throws BowlingException{
		Game game = new Game();
		
		game.setFirstBonusThrow(7);
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		
		assertEquals(90,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnSecondBonusThrow() throws BowlingException{
		Game game = new Game();
		
		game.setSecondBonusThrow(9);

		assertEquals(9,game.getSecondBonusThrow());
	}
	
	@Test
	public void testGameScoreWithStrikeAsTheLastFrame() throws BowlingException{
		Game game = new Game();
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		
		assertEquals(92,game.calculateScore());
	}
	
	@Test
	public void testGameScoreWithBestScore() throws BowlingException{
		Game game = new Game();
			
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		
		assertEquals(300,game.calculateScore());
	}
}
