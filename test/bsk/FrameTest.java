package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testShouldReturnFirstThrow() throws BowlingException{
		int firstThrow = 1;
		
		Frame frame = new Frame(firstThrow, 2);
		
		assertEquals(firstThrow, frame.getFirstThrow());
	}
	
	@Test
	public void testShouldReturnSecondThrow() throws BowlingException{
		int secondThrow = 2;
		
		Frame frame = new Frame(1, secondThrow);
		
		assertEquals(secondThrow, frame.getSecondThrow());
	}
	
	@Test
	public void testShouldReturnBothThrow() throws BowlingException{
		int firstThrow = 2;
		int secondThrow = 4;
		
		Frame frame = new Frame(firstThrow, secondThrow);
		
		assertEquals(firstThrow,frame.getFirstThrow());
		assertEquals(secondThrow, frame.getSecondThrow());
		
	}
	
	@Test
	public void testShouldReturnScore() throws BowlingException{
		Frame frame = new Frame(2,6);
		
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void testShouldBeTrueIfSpare() throws BowlingException{
		Frame frame = new Frame(4,6);
		
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testShouldBeTrueIfStrike() throws BowlingException{
		Frame frame = new Frame(10,0);
		
		assertTrue(frame.isStrike());
	}
}
