package tdd.training.bsk;

import java.util.ArrayList;
import java.util.Collection;

public class Game {
	ArrayList<Frame> frames;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;
	private static int LAST_FRAME = 9;
	private static int DIM = 10;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
		frames = new ArrayList<Frame>(DIM);
		
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		frames.add(frame);
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		
		return frames.get(index);
		
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		int i = 0;
		
		for(i = 0;i<frames.size();i++) {
			
			Frame frame = frames.get(i);
		
			if(frames.get(i).isStrike() && i == LAST_FRAME-1) { 
			
				frame.setBonus(frames.get(i+1).getFirstThrow() + firstBonusThrow);
			
			}else if(frames.lastIndexOf(frame) == LAST_FRAME) {
				
				frame.setBonus(firstBonusThrow + secondBonusThrow); //secondBonusThrow will be 0 if the last frame its not a strike
					
			}else if(frames.get(i).isSpare() && i < LAST_FRAME) {
				
				frame.setBonus(frames.get(i+1).getFirstThrow());
				
			}else if( frames.get(i).isStrike() && i < LAST_FRAME){
				
				if( frames.get(i+1).isStrike() ) {
					
					frame.setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+2).getFirstThrow() );
					
				}else {
					
						frame.setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow());
						
				}
				
			}
		
				score = score + frame.getScore();
			
		}
		
		return score;	
	}

}
